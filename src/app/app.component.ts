import { Component } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, finalize } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // fileRef;
  task;

  uploadProgress;

  downloadURL;

  uploadState

  constructor(private afStorage: AngularFireStorage) { }
  upload(event) {
    // this.afStorage.upload('/upload/to/this-path', event.target.files[0]);  
    const file = event.target.files[0];
    const filePath = Math.random().toString(36).substring(2);
    const fileRef = this.afStorage.ref(filePath);

    // this.task = this.ref.put(event.target.files[0]);
    this.task = this.afStorage.upload(filePath, file);

    // this.uploadProgress = this.task.snapshotChanges()
    // .pipe(map((s:any) => (s.bytesTransferred / s.totalBytes) * 100));
    this.uploadProgress = this.task.percentageChanges();

    this.uploadState = this.task.snapshotChanges().pipe(map((s: any) => s.state));

    // this.downloadURL = this.ref.getDownloadURL();
    // fileRef.getDownloadURL().subscribe(ref => {
    //   console.log('REF', ref)
    //   this.downloadURL = ref
    // })
    // console.log(this.downloadURL);

    this.task.snapshotChanges().pipe(
      finalize(() => {this.downloadURL = this.afStorage.ref(filePath).getDownloadURL()
      console.log(this.downloadURL);
      } )
   )
  .subscribe()

  }
}
