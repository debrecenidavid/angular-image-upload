import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularFireModule} from '@angular/fire'
// import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyB9TP1Sxf2-7n_fFxbuRIzmv38PTpmvAG8",
      authDomain: "imageupload-784f3.firebaseapp.com",
      databaseURL: "https://imageupload-784f3.firebaseio.com",
      projectId: "imageupload-784f3",
      storageBucket: "imageupload-784f3.appspot.com",
      messagingSenderId: "48498665210"
    })
  ],
  // providers: [AngularFireStorage],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
